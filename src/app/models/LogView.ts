export type LogView = {
	id: number;
	Description?: string;
	Status: LogView.logStatus;
	IdService: number;
	IdUser: number;
};

// tslint:disable-next-line:no-namespace
export namespace LogView {

	export enum logStatus {
		SUCCESS = 'Success',
		FAILURE = 'Failure',
	}
}
