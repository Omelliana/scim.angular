import { Component, Input, OnInit } from '@angular/core';
import { LogView } from '../../../models';

@Component({
	selector: 'sync-row',
	templateUrl: './row.component.html',
	styleUrls: ['./row.component.sass']
})
export class RowComponent implements OnInit{

	images = ['assets/img/gitlab.svg', 'assets/img/youtrack.svg'];
	@Input() data!: LogView;
	logStatus = LogView.logStatus;

	constructor() {
	}

	ngOnInit(): void {
		console.log(this.data);
	}
}
