import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from './services/local-storage.service';
import { AuthService } from './services/auth.service';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {
	title = 'frontend-diplom';

	constructor(
		private auth: AuthService,
		private localStorageService: LocalStorageService
	) {
	}

	ngOnInit(): void {
		const token = this.localStorageService.get();
		if (token) {
			this.auth.token = token;
		}

	}
}
