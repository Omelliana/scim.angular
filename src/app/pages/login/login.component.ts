import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { AuthService } from '../../services/auth.service';

@Component({
	selector: 'sync-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.sass'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginComponent implements OnDestroy {

	hide = true;
	form!: FormGroup;
	subscription!: Subscription;
	isLoading = false;
	showError = false;
	errorMessage = '';

	constructor(
		private fb: FormBuilder,
		private cdr: ChangeDetectorRef,
		private authService: AuthService
	) {
		this.form = this.fb.group({
			login: ['', Validators.required],
			password: ['', Validators.required],
		});
	}

	loginUser(): void {
		this.isLoading = true;
		this.subscription = this.authService.loginUser(this.form.value)
			.subscribe(
				(res) => {
					this.authService.authenticate(res.token);
					this.isLoading = false;
					this.showError = false;
					this.cdr.markForCheck();
				}, err => {
					this.showError = true;
					this.isLoading = false;
					this.errorMessage = err.status === 404 ? 'Логин не верен' : 'Пароль не верен';
					this.cdr.markForCheck();
				});
	}

	ngOnDestroy(): void {
		if (this.subscription) {
			this.subscription.unsubscribe();
		}
	}
}
