import { ChangeDetectionStrategy, Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LogView } from '../../models';
import { Observable } from 'rxjs';
import { LogsService } from '../../services/logs.service';

export enum Service {
	All,
	GitLab,
	YouTrack
}

@Component({
	selector: 'sync-main',
	templateUrl: './main.component.html',
	styleUrls: ['./main.component.sass'],
	changeDetection: ChangeDetectionStrategy.OnPush
})

export class MainComponent {

	logs$: Observable<Array<LogView>> = this.logsService.getAllLogs();
	filter = Service;
	clicked = this.filter.All;

	constructor(
		private httpClient: HttpClient,
		private logsService: LogsService,
	) {
	}

	// All
	getAllLogs(): void {
		this.clicked = this.filter.All;
		this.logs$ = this.logsService.getAllLogs();
	}

	// GitLab
	getGitLAbLogs(): void {
		this.clicked = this.filter.GitLab;
		this.logs$ = this.logsService.getServiceLogs(1);
	}

	// YouTrack
	getYouTrackLogs(): void {
		this.clicked = this.filter.YouTrack;
		this.logs$ = this.logsService.getServiceLogs(2);
	}
}
