import { Injectable } from '@angular/core';

@Injectable({
	providedIn: 'root'
})
export class LocalStorageService {
	constructor() {
	}

	set(value: string): void {
		localStorage.setItem('token', value);
	}

	get(): string | null {
		return localStorage.getItem('token');
	}

	remove(): void {
		localStorage.removeItem('token');
	}
}
