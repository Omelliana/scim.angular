import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Admin } from '../models';
import { environment } from '../../environments/environment.prod';
import { LocalStorageService } from './local-storage.service';
import { Observable } from 'rxjs';


@Injectable({
	providedIn: 'root'
})
export class AuthService {

	tokenString = '';

	constructor(
		private http: HttpClient,
		private localStorageService: LocalStorageService
	) {
	}

	get token(): string{
		return this.tokenString;
	}

	set token(newToken: string) {
		this.tokenString = newToken;
	}

	loginUser(user: Admin): Observable<any> {
		return this.http.post<any>(`${environment.apiUrl}/auth/login`, user);
	}

	logout(): void {
		this.localStorageService.remove();
	}

	authenticate(value: string): void {
		this.localStorageService.set(value);
		this.tokenString = value;
	}
}
