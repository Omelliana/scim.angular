import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LogView } from '../models';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment.prod';
import { map } from 'rxjs/operators';

@Injectable({
	providedIn: 'root'
})
export class LogsService {

	constructor(private http: HttpClient) {
	}

	getAllLogs(): Observable<Array<LogView>> {
		return this.http.get<Array<LogView>>(`${environment.apiUrl}/logs`);
	}

	getServiceLogs(id: number): Observable<Array<LogView>> {
		return this.getAllLogs().pipe(
			map(logs => logs.filter(log => log.IdService === id))
		);
		// return this.getAllLogs().pipe(filter(log => log.filter()));
	}
}
